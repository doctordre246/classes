#include <iostream>
#include "Demo.h" //include class defintion

using namespace std;

void mymovies(OnlineVid var);


int main()
	{
			
		OnlineVid d1, d2; //initialized by default constructor
		OnlineVid d3 (34, "Happy Feet"); //initialized with overloaded constructor
		OnlineVid d4 = {24, "Madea"};//different syntax for overloaded constructor
		
		cout<<"d1.views = "<<d1.getViews()<<endl;
		cout<<"d3.views = "<<d3.getViews()<<endl;
		cout<<"d4.views = "<<d4.getViews()<<endl;
		
		//d2.views = 5; //error ("views" is private)
		
		d2.setViews(5);//correct way
		
		d2 = d3;//default assignment
		
		//however, by default we cannot print
		//cout<<d1; //error
		
		mymovies(d2);//pass d2 as an argument
		
		cout<<endl<<"Welcome to class Onlinevid...."<<endl;
		
		
		
		cout<<"d2.views = "<<d2.getViews()<<endl;
		cout<<"d1.views = "<<d1.getViews()<<endl;
		cout<<"d2.views = "<<d2.getViews()<<endl;
		cout<<"d1.views = "<<d1.getViews()<<endl;
		cout<<"d3.views = "<<d3.getViews()<<endl;
		//cout<<"d4.views = "<<d4.getViews()<<endl;
		
		
				
	}
	void mymovies(OnlineVid a)
	{
		cout<<"fun with 'a'"<<endl;
	}

