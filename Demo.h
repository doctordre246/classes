#include <iostream>
#include <string>

using namespace std;

class OnlineVid
 {
    private:  //properties
         unsigned  views;
         string id; 
         


    public:  //methods
        
         OnlineVid();//default constructor 
         OnlineVid(unsigned, string);//overloaded constructor
			
		  void movies();
          void setViews(unsigned);//set value of views
          unsigned getViews();//view content of the variable views
          void setId(string);
          string getId();

 };

